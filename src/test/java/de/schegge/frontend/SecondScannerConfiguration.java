package de.schegge.frontend;

import org.springframework.context.annotation.Configuration;

@Configuration
@FrontendScan(basePackageClasses = {de.schegge.test.notempty.Dto.class})
class SecondScannerConfiguration {

}