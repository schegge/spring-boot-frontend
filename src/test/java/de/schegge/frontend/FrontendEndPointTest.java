package de.schegge.frontend;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

@SpringBootTest(classes = { FrontendProperties.class, FrontendEndPoint.class, ValidationsConfiguration.class,
    HibernateConfiguration.class }, properties = { "management.endpoints.web.exposure.include=*",
        "logging.level.de.schegge.frontend=DEBUG", "de.schegge.frontend.add-message=true" })
@AutoConfigureMockMvc
@EnableAutoConfiguration(exclude = { FrontendAutoConfiguration.class })
public class FrontendEndPointTest {
  @Autowired
  FrontendEndPoint endpoint;

  @Autowired
  private MockMvc mvc;

  @Autowired
  ApplicationContext ctx;

  @Frontend("test")
  static class TestDto {
    @NotEmpty
    String description;
    @NotBlank(message = "name is xxx")
    String name;
  }

  @Frontend
  static class TsetDto {
    @Min(9)
    int test;
  }

  @Test
  void actuatorFrontEnd() throws Exception {
    mvc.perform(get("/actuator/frontend")).andDo(MockMvcResultHandlers.print()).andExpect(content().json("{" //
        + "\"test\":[" //
        + "{\"nonEmpty\":true,\"message\":\"{javax.validation.constraints.NotEmpty.message}\",\"name\":\"description\",\"type\":\"String\"}," //
        + "{\"nonBlank\":true,\"message\":\"name is xxx\",\"name\":\"name\",\"type\":\"String\"}" //
        + "]," //
        + "\"TsetDto\":[" //
        + "{\"min\":9,\"message\":\"{javax.validation.constraints.Min.message}\",\"name\":\"test\",\"type\":\"int\"}" //
        + "]" //
        + "}", //
        true)).andExpect(status().isOk());
  }

  @Test
  void actuatorFrontendTestDto() throws Exception {
    mvc.perform(get("/actuator/frontend/test")).andDo(MockMvcResultHandlers.print()).andExpect(content().json("[" //
        + "{\"nonEmpty\":true,\"message\":\"{javax.validation.constraints.NotEmpty.message}\",\"name\":\"description\",\"type\":\"String\"}," //
        + "{\"nonBlank\":true,\"message\":\"name is xxx\",\"name\":\"name\",\"type\":\"String\"}" //
        + "]", true)).andExpect(status().isOk());
  }
}
