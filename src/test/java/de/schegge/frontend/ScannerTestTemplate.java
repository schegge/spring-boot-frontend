package de.schegge.frontend;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Set;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;

@EnableAutoConfiguration(exclude = FrontendAutoConfiguration.class)
interface ScannerTestTemplate {

  Set<Class<?>> getClasses();

  @Test
  default void actuatorFrontEnd(@Autowired ApplicationContext applicationContext) {
    FrontendScanner scanner = new FrontendScanner(applicationContext);
    assertEquals(getClasses(), scanner.scan());
  }
}
