package de.schegge.frontend.feature;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;

import de.schegge.frontend.Validations;
import de.schegge.frontend.feature.ValidationsFeature;

class ValidationsFeatureTest {

  @Test
  void attach() {
    final Validations validations = mock(Validations.class);
    new ValidationsFeature().attach(validations);
    verify(validations, times(16)).addAnnotationMapper(any(), any());
  }
}