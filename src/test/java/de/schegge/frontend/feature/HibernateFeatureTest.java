package de.schegge.frontend.feature;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.schegge.frontend.Validations;
import org.junit.jupiter.api.Test;

class HibernateFeatureTest {

  @Test
  void attach() {
    final Validations validations = mock(Validations.class);
    new HibernateFeature().attach(validations);
    verify(validations, times(8)).addAnnotationMapper(any(), any());
  }
}