package de.schegge.frontend.feature;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;

import de.schegge.frontend.Validations;

class UriValidationFeatureTest {

  @Test
  void attach() {
    final Validations validations = mock(Validations.class);
    new UriValidationFeature().attach(validations);
    verify(validations, times(3)).addAnnotationMapper(any(), any());
  }
}