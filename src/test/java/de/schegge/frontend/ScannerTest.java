package de.schegge.frontend;

import static java.util.Collections.singleton;

import java.util.Set;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = ScannerConfiguration.class, properties = "management.endpoints.web.exposure.include=*")
class ScannerTest implements ScannerTestTemplate {

  @Override
  public Set<Class<?>> getClasses() {
    return singleton(de.schegge.test.notempty.Dto.class);
  }
}
