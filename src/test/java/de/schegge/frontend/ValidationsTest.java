package de.schegge.frontend;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.junit.jupiter.api.Test;

import de.schegge.frontend.feature.AnnotationFeature;
import de.schegge.frontend.mapper.ConstantAnnotationMapper;

class ValidationsTest {

  private static class DtoWithConstraint {

    @NotNull
    String label;
    @Deprecated
    Integer test;
    @NotEmpty
    List<DtoWithConstraint> selfs;
  }

  @Test
  void readKnownAnnotation() {
    Validations validations = new Validations();
    validations.addAnnotationMapper(NotNull.class, new ConstantAnnotationMapper("nonNull", true));
    final List<ConstraintInfo> constraintInfos = validations.read(DtoWithConstraint.class);
    assertEquals(1, constraintInfos.size());
    final Map<String, Object> details = constraintInfos.get(0).getDetails();
    assertAll(() -> assertEquals(3, details.size()), () -> assertEquals(Boolean.TRUE, details.get("nonNull")),
        () -> assertEquals("label", details.get("name")), () -> assertEquals("String", details.get("type")));
  }

  @Test
  void readKnownListAnnotation() {
    Validations validations = new Validations();
    validations.addFeature(new AnnotationFeature() {

      @Override
      public void attach(Validations validation) {
        validation.addAnnotationMapper(NotEmpty.class, new ConstantAnnotationMapper("nonEmpty", true));

      }
    });
    final List<ConstraintInfo> constraintInfos = validations.read(DtoWithConstraint.class);
    assertEquals(1, constraintInfos.size());
    final Map<String, Object> details = constraintInfos.get(0).getDetails();
    assertAll(() -> assertEquals(4, details.size()), () -> assertEquals(Boolean.TRUE, details.get("nonEmpty")),
        () -> assertEquals("DtoWithConstraint", details.get("component")),
        () -> assertEquals("selfs", details.get("name")), () -> assertEquals("List", details.get("type")));
  }

  @Test
  void readUnknownAnnotation() {
    Validations validations = new Validations();
    final List<ConstraintInfo> constraintInfos = validations.read(DtoWithConstraint.class);
    assertTrue(constraintInfos.isEmpty());
  }
}