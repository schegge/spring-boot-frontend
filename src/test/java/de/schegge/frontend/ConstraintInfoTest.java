package de.schegge.frontend;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class ConstraintInfoTest {

  @Test
  void testEmptyBuilder() {
    ConstraintInfo.Builder builder = new ConstraintInfo.Builder();
    assertTrue(builder.isEmpty());
    assertTrue(builder.build().getDetails().isEmpty());
  }
}
