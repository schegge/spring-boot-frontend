package de.schegge.frontend;

import de.schegge.frontend.FrontendEndPointTest.TestDto;
import de.schegge.frontend.FrontendEndPointTest.TsetDto;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = {ScannerConfiguration.class, EmptyScannerConfiguration.class,
    SecondScannerConfiguration.class}, properties = "management.endpoints.web.exposure.include=*")
class ScannerWithMultipleConfigurationsTest implements ScannerTestTemplate {

  @Override
  public Set<Class<?>> getClasses() {
    return new HashSet<>(Arrays.asList(TsetDto.class, TestDto.class, de.schegge.test.notempty.Dto.class));
  }
}
