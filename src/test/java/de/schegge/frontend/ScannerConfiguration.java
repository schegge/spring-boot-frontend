package de.schegge.frontend;

import org.springframework.context.annotation.Configuration;

@Configuration
@FrontendScan(basePackageClasses = { de.schegge.test.empty.Dto.class, de.schegge.test.notempty.Dto.class })
class ScannerConfiguration {

}