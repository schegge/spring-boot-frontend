package de.schegge.frontend.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.lang.reflect.Field;
import java.util.Map;

import javax.validation.constraints.Min;

import org.junit.jupiter.api.Test;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ReflectionUtils;

import de.schegge.frontend.ConstraintInfo.Builder;

class ValueAttributesMapperTest {
  private static class TestDto {
    @Min(value = 2, message = "must greater then 1")
    public String member;
  }

  @Test
  void test() {
    Field field = ReflectionUtils.findField(TestDto.class, "member");
    Min annotation = AnnotationUtils.findAnnotation(field, Min.class);
    AnnotationAttributes attributes = AnnotationUtils.getAnnotationAttributes(null, annotation);
    Builder builder = new Builder();

    ValueAttributesMapper mapper = new ValueAttributesMapper("max");
    mapper.modify(annotation, attributes, builder);

    Map<String, Object> details = builder.build().getDetails();
    assertEquals(2, details.size());
    assertEquals(2L, details.get("max"));
    assertEquals("must greater then 1", details.get("message"));
  }
}
