package de.schegge.frontend.mapper;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.lang.reflect.Field;
import java.util.Map;

import javax.validation.constraints.Size;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ReflectionUtils;

import de.schegge.frontend.ConstraintInfo.Builder;

class AnnotationAttributesMapperTest {
  private static class TestDto {
    @Size(min = 2, max = 12)
    public String member1;
    @Size(max = 22)
    public String member2;
    @Size(min = 12)
    public String member3;
  }

  private AnnotationAttributesMapper mapper;
  private Builder builder;

  @BeforeEach
  void beforeEach() {
    mapper = new AnnotationAttributesMapper();
    builder = new Builder();
  }

  @Test
  void testSize() {
    Field field = ReflectionUtils.findField(TestDto.class, "member1");
    Size annotation = AnnotationUtils.findAnnotation(field, Size.class);
    AnnotationAttributes attributes = AnnotationUtils.getAnnotationAttributes(null, annotation);
    mapper.modify(annotation, attributes, builder);

    Map<String, Object> details = builder.build().getDetails();
    assertAll(() -> assertEquals(2, details.get("min")), () -> assertEquals(12, details.get("max")));
  }

  @Test
  void testSizeWithMinDefaultValue() {
    Field field = ReflectionUtils.findField(TestDto.class, "member2");
    Size annotation = AnnotationUtils.findAnnotation(field, Size.class);
    AnnotationAttributes attributes = AnnotationUtils.getAnnotationAttributes(null, annotation);

    mapper.modify(annotation, attributes, builder);

    Map<String, Object> details = builder.build().getDetails();
    assertAll(() -> assertEquals(5, details.size()), () -> assertEquals(0, details.get("min")),
        () -> assertEquals(22, details.get("max")));
  }

  @Test
  void testSizeWithMaxDefaultValue() {
    Field field = ReflectionUtils.findField(TestDto.class, "member3");
    Size annotation = AnnotationUtils.findAnnotation(field, Size.class);
    AnnotationAttributes attributes = AnnotationUtils.getAnnotationAttributes(null, annotation);

    mapper.modify(annotation, attributes, builder);

    Map<String, Object> details = builder.build().getDetails();
    assertAll(() -> assertEquals(5, details.size()), () -> assertEquals(12, details.get("min")),
        () -> assertEquals(Integer.MAX_VALUE, details.get("max")));
  }
}
