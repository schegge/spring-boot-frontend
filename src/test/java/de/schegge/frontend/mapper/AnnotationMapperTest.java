package de.schegge.frontend.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.lang.reflect.Field;

import javax.validation.constraints.Size;

import org.junit.jupiter.api.Test;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ReflectionUtils;

import de.schegge.frontend.ConstraintInfo;
import de.schegge.frontend.ConstraintInfo.Builder;

public class AnnotationMapperTest {
  private static class TestDto {
    @Size(min = 2, max = 12)
    public String member;
  }

  @Test
  void andThen() {
    Field field = ReflectionUtils.findField(TestDto.class, "member");
    Size annotation = AnnotationUtils.findAnnotation(field, Size.class);
    AnnotationAttributes attributes = AnnotationUtils.getAnnotationAttributes(null, annotation);

    Builder builder = new Builder();
    ConstantAnnotationMapper mapperA = new ConstantAnnotationMapper("a", "A");
    ConstantAnnotationMapper mapperB = new ConstantAnnotationMapper("b", "B");
    AnnotationMappper mapper = mapperA.andThen(mapperB);
    mapper.modify(annotation, attributes, builder);
    ConstraintInfo constraintInfo = builder.build();
    assertEquals("A", constraintInfo.getDetails().get("a"));
    assertEquals("B", constraintInfo.getDetails().get("b"));
  }
}