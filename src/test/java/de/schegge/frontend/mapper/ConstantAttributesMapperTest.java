package de.schegge.frontend.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.lang.reflect.Field;
import java.util.Map;

import javax.validation.constraints.Size;

import org.junit.jupiter.api.Test;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ReflectionUtils;

import de.schegge.frontend.ConstraintInfo.Builder;

class ConstantAttributesMapperTest {
  private static class TestDto {
    @Size(min = 2, max = 12)
    public String member;
  }

  @Test
  void test() {
    Field field = ReflectionUtils.findField(TestDto.class, "member");
    Size annotation = AnnotationUtils.findAnnotation(field, Size.class);
    AnnotationAttributes attributes = AnnotationUtils.getAnnotationAttributes(null, annotation);
    Builder builder = new Builder();

    ConstantAnnotationMapper mapper = new ConstantAnnotationMapper("key", true);
    mapper.modify(annotation, attributes, builder);

    Map<String, Object> details = builder.build().getDetails();
    assertEquals(2, details.size());
    assertEquals(Boolean.TRUE, details.get("key"));
    assertEquals("{javax.validation.constraints.Size.message}", details.get("message"));
  }
}
