package de.schegge.test.notempty;

import javax.validation.constraints.Max;

import de.schegge.frontend.Frontend;

@Frontend
public class Dto {

  @Max(42)
  Long length;
}
