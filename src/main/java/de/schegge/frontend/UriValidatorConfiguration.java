package de.schegge.frontend;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.schegge.frontend.feature.AnnotationFeature;
import de.schegge.frontend.feature.UriValidationFeature;
import de.schegge.validator.Localhost;

@Configuration
@ConditionalOnClass(Localhost.class)
public class UriValidatorConfiguration {
  @Bean("uri-validator")
  AnnotationFeature getFeature() {
    return new UriValidationFeature();
  }
}
