package de.schegge.frontend;

import org.springframework.boot.actuate.autoconfigure.endpoint.EndpointAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.info.ConditionalOnEnabledInfoContributor;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AutoConfigureBefore(EndpointAutoConfiguration.class)
@ConditionalOnClass({ InfoContributor.class, ConditionalOnEnabledInfoContributor.class })
public class FrontendAutoInfoConfiguration {

  @Bean
  @ConditionalOnEnabledInfoContributor("frontend")
  public FrontendInfoContributor flowableInfoContributor() {
    return new FrontendInfoContributor();
  }
}
