package de.schegge.frontend;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_DEFAULT)
public final class ConstraintInfo {
  private final Map<String, Object> details;

  private ConstraintInfo(Builder builder) {
    Map<String, Object> content = new LinkedHashMap<>();
    content.putAll(builder.content);
    details = Collections.unmodifiableMap(content);
  }

  @JsonAnyGetter
  public Map<String, Object> getDetails() {
    return details;
  }

  public static class Builder {
    private final Map<String, Object> content;

    public Builder() {
      content = new LinkedHashMap<>();
    }

    public Builder withDetail(String key, Object value) {
      content.put(key, value);
      return this;
    }

    public Builder withDetails(Map<String, Object> details) {
      content.putAll(details);
      return this;
    }

    public void removeAll(String... keys) {
      for (String key : keys) {
        content.remove(key);
      }

    }

    public boolean isEmpty() {
      return content.isEmpty();
    }

    public ConstraintInfo build() {
      return new ConstraintInfo(this);
    }
  }
}
