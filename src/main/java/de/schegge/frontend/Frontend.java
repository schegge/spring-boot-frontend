package de.schegge.frontend;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;

@Retention(RUNTIME)
@Target(ElementType.TYPE)
public @interface Frontend {
	@AliasFor("name")
	String value() default "";

	@AliasFor("value")
	String name() default "";

}
