package de.schegge.frontend.feature;

import de.schegge.frontend.Validations;

public interface AnnotationFeature {
  void attach(Validations validation);
}
