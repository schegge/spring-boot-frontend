package de.schegge.frontend.feature;

import de.schegge.frontend.Validations;
import de.schegge.frontend.mapper.AnnotationAttributesMapper;
import de.schegge.frontend.mapper.ConstantAnnotationMapper;
import de.schegge.validator.Host;
import de.schegge.validator.Localhost;
import de.schegge.validator.NotLocalhost;

public class UriValidationFeature implements AnnotationFeature {

  @Override
  public void attach(Validations validation) {
    validation.addAnnotationMapper(Host.class, new AnnotationAttributesMapper());
    validation.addAnnotationMapper(Localhost.class, new ConstantAnnotationMapper("host", "localhost"));
    validation.addAnnotationMapper(NotLocalhost.class,
        new ConstantAnnotationMapper("host", "localhost").andThen(new ConstantAnnotationMapper("negate", "true")));
  }
}
