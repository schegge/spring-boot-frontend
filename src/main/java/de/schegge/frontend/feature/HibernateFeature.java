package de.schegge.frontend.feature;

import org.hibernate.validator.constraints.CreditCardNumber;
import org.hibernate.validator.constraints.EAN;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.ISBN;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.SafeHtml;

import de.schegge.frontend.Validations;
import de.schegge.frontend.mapper.AnnotationAttributesMapper;
import de.schegge.frontend.mapper.ConstantAnnotationMapper;

public class HibernateFeature implements AnnotationFeature {

  private static final AnnotationAttributesMapper ANNOTATION_ATTRIBUTES_MAPPER = new AnnotationAttributesMapper();

  @Override
  public void attach(Validations validation) {
    validation.addAnnotationMapper(SafeHtml.class, new ConstantAnnotationMapper("format", "HTML"));
    validation.addAnnotationMapper(NotBlank.class, new ConstantAnnotationMapper("nonBlank", true));
    validation.addAnnotationMapper(NotEmpty.class, new ConstantAnnotationMapper("nonEmpty", true));
    validation.addAnnotationMapper(EAN.class,
        new ConstantAnnotationMapper("format", "EAN").andThen(ANNOTATION_ATTRIBUTES_MAPPER));
    validation.addAnnotationMapper(Length.class, ANNOTATION_ATTRIBUTES_MAPPER);
    validation.addAnnotationMapper(ISBN.class,
        new ConstantAnnotationMapper("format", "ISBN").andThen(ANNOTATION_ATTRIBUTES_MAPPER));
    validation.addAnnotationMapper(Email.class,
        new ConstantAnnotationMapper("format", "EMAIL").andThen(ANNOTATION_ATTRIBUTES_MAPPER));
    validation.addAnnotationMapper(CreditCardNumber.class,
        new ConstantAnnotationMapper("format", "Creditcard").andThen(ANNOTATION_ATTRIBUTES_MAPPER));
  }
}
