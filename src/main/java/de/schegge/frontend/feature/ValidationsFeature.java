package de.schegge.frontend.feature;

import javax.validation.constraints.Email;
import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Negative;
import javax.validation.constraints.NegativeOrZero;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import de.schegge.frontend.Validations;
import de.schegge.frontend.mapper.AnnotationAttributesMapper;
import de.schegge.frontend.mapper.AnnotationMappper;
import de.schegge.frontend.mapper.ConstantAnnotationMapper;
import de.schegge.frontend.mapper.ValueAttributesMapper;

public class ValidationsFeature implements AnnotationFeature {

  private final static AnnotationMappper FUTURE = new ConstantAnnotationMapper("future", true);
  private final static AnnotationMappper PRESENT = new ConstantAnnotationMapper("present", true);
  private final static AnnotationMappper PAST = new ConstantAnnotationMapper("past", true);

  @Override
  public void attach(Validations validation) {
    validation.addAnnotationMapper(NotNull.class, new ConstantAnnotationMapper("mandatory", true));
    validation.addAnnotationMapper(NotEmpty.class, new ConstantAnnotationMapper("nonEmpty", true));
    validation.addAnnotationMapper(NotBlank.class, new ConstantAnnotationMapper("nonBlank", true));
    validation.addAnnotationMapper(Positive.class, new ConstantAnnotationMapper("min", 1));
    validation.addAnnotationMapper(PositiveOrZero.class, new ConstantAnnotationMapper("min", 0));
    validation.addAnnotationMapper(Negative.class, new ConstantAnnotationMapper("max", -1));
    validation.addAnnotationMapper(NegativeOrZero.class, new ConstantAnnotationMapper("max", 0));
    validation.addAnnotationMapper(Future.class, FUTURE.andThen(PRESENT));
    validation.addAnnotationMapper(FutureOrPresent.class, FUTURE.andThen(PRESENT));
    validation.addAnnotationMapper(Past.class, PAST);
    validation.addAnnotationMapper(PastOrPresent.class, PAST.andThen(PRESENT));
    validation.addAnnotationMapper(Min.class, new ValueAttributesMapper("min"));
    validation.addAnnotationMapper(Max.class, new ValueAttributesMapper("max"));
    validation.addAnnotationMapper(Pattern.class,
        new ConstantAnnotationMapper("format", "Pattern").andThen(new AnnotationAttributesMapper()));
    validation.addAnnotationMapper(Size.class, new AnnotationAttributesMapper());
    validation.addAnnotationMapper(Email.class,
        new ConstantAnnotationMapper("format", "Email").andThen(new AnnotationAttributesMapper()));

  }

}
