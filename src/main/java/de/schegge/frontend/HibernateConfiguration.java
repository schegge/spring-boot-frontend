package de.schegge.frontend;

import org.hibernate.validator.HibernateValidator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.schegge.frontend.feature.AnnotationFeature;
import de.schegge.frontend.feature.HibernateFeature;

@Configuration
@ConditionalOnClass(HibernateValidator.class)
public class HibernateConfiguration {
  @Bean("hibernate")
  AnnotationFeature getFeature() {
    return new HibernateFeature();
  }
}
