package de.schegge.frontend.mapper;

import java.lang.annotation.Annotation;
import java.util.Objects;

import org.springframework.core.annotation.AnnotationAttributes;

import de.schegge.frontend.ConstraintInfo.Builder;

@FunctionalInterface
public interface AnnotationMappper {
  public void modify(Annotation annotation, AnnotationAttributes attributes, Builder constraint);

  default AnnotationMappper andThen(AnnotationMappper after) {
    Objects.requireNonNull(after);
    return (Annotation a, AnnotationAttributes aa, Builder c) -> {
      modify(a, aa, c);
      after.modify(a, aa, c);
    };
  }

  static AnnotationMappper noop() {
    return (Annotation a, AnnotationAttributes aa, Builder c) -> {
    };
  }
}
