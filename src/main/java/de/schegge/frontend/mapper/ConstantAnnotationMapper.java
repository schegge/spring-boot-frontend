/*
 * ConstantAnnotationMapper.java
 *
 * Author: KAIS114
 * Copyright (c) 2019 arvato
 */
package de.schegge.frontend.mapper;

import java.lang.annotation.Annotation;

import org.springframework.core.annotation.AnnotationAttributes;

import de.schegge.frontend.ConstraintInfo.Builder;

public class ConstantAnnotationMapper implements AnnotationMappper {

  private final Object value;
  private final String name;

  public ConstantAnnotationMapper(String name, Object value) {
    this.name = name;
    this.value = value;
  }

  @Override
  public void modify(Annotation annotation, AnnotationAttributes attributes, Builder constraint) {
    constraint.withDetail(name, value);
    constraint.withDetail("message", attributes.get("message"));
  }
}
