/*
 * AnnotationAttributesMapper.java
 *
 * Author: KAIS114
 * Copyright (c) 2019 arvato
 */
package de.schegge.frontend.mapper;

import java.lang.annotation.Annotation;

import org.springframework.core.annotation.AnnotationAttributes;

import de.schegge.frontend.ConstraintInfo.Builder;

public class AnnotationAttributesMapper implements AnnotationMappper {

  @Override
  public void modify(Annotation annotation, AnnotationAttributes attributes, Builder constraint) {
    constraint.withDetails(attributes);
  }
}
