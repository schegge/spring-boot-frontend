package de.schegge.frontend;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Constraint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.ReflectionUtils.FieldCallback;

import de.schegge.frontend.feature.AnnotationFeature;
import de.schegge.frontend.mapper.AnnotationMappper;

public class Validations {
  private static final Logger LOGGER = LoggerFactory.getLogger(Validations.class);

  private Map<Class<? extends Annotation>, AnnotationMappper> creator = new HashMap<>();
  private boolean addMessage;

  private final class ContrainedFieldCallback implements FieldCallback {
    private List<ConstraintInfo> fieldConstraints = new ArrayList<>();

    @Override
    public void doWith(Field field) throws IllegalArgumentException {
      ConstraintInfo.Builder builder = new ConstraintInfo.Builder();
      for (Annotation annotation : AnnotationUtils.getAnnotations(field)) {
        AnnotationAttributes attributes = AnnotationUtils.getAnnotationAttributes(null, annotation);
        if (AnnotationUtils.findAnnotation(annotation.getClass(), Constraint.class) != null) {
          creator.getOrDefault(annotation.annotationType(), AnnotationMappper.noop()).modify(annotation, attributes, builder);
        }
      }
      builder.removeAll("payload", "groups");
      if (!addMessage) {
        builder.removeAll("message");
      }
      if (builder.isEmpty()) {
        return;
      }
      builder.withDetail("name", field.getName());
      builder.withDetail("type", field.getType().getSimpleName());
      if (field.getType().isAssignableFrom(List.class)) {
        Type type = ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
        builder.withDetail("component", ((Class<?>) type).getSimpleName());
      }
      fieldConstraints.add(builder.build());
    }
  }

  public void addAnnotationMapper(Class<? extends Annotation> annotation, AnnotationMappper fieldConstraintCreator) {
    creator.put(annotation, fieldConstraintCreator);
  }

  public void addFeature(AnnotationFeature validationsSupport) {
    LOGGER.debug("add: {}", validationsSupport);
    validationsSupport.attach(this);
  }

  public List<ConstraintInfo> read(Class<?> dto) {
    ContrainedFieldCallback callback = new ContrainedFieldCallback();
    ReflectionUtils.doWithFields(dto, callback);

    return callback.fieldConstraints;
  }

  public boolean isAddMessage() {
    return addMessage;
  }

  public void setAddMessage(boolean addMessage) {
    this.addMessage = addMessage;
  }
}
