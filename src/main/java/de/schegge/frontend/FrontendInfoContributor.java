package de.schegge.frontend;

import static java.util.Collections.singletonMap;

import org.springframework.boot.actuate.info.Info.Builder;
import org.springframework.boot.actuate.info.InfoContributor;

public class FrontendInfoContributor implements InfoContributor {

  @Override
  public void contribute(Builder builder) {
    builder.withDetail("frontend",
        singletonMap("version", FrontendInfoContributor.class.getPackage().getImplementationVersion()));
  }
}
