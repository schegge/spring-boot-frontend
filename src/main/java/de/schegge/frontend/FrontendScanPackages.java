package de.schegge.frontend;

import static java.util.Collections.unmodifiableList;
import static java.util.stream.Collectors.toList;
import static org.springframework.util.StringUtils.concatenateStringArrays;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.util.ClassUtils;
import org.springframework.util.StringUtils;

public class FrontendScanPackages {
	private static final String BEAN = FrontendScanPackages.class.getName();
	private static final String FRONTEND_SCAN_NAME = FrontendScan.class.getName();

	private static final FrontendScanPackages NONE = new FrontendScanPackages();

	private final List<String> packageNames;

	FrontendScanPackages(String... packageNames) {
		this.packageNames = Arrays.stream(packageNames).filter(StringUtils::hasText).collect(toList());
	}

	public List<String> getPackageNames() {
		return unmodifiableList(packageNames);
	}

	public static FrontendScanPackages get(BeanFactory beanFactory) {
		try {
			return beanFactory.getBean(BEAN, FrontendScanPackages.class);
		} catch (NoSuchBeanDefinitionException ex) {
			return NONE;
		}
	}

	public static void register(BeanDefinitionRegistry registry, String... packageNames) {
		if (registry.containsBeanDefinition(BEAN)) {
			BeanDefinition beanDefinition = registry.getBeanDefinition(BEAN);
			ConstructorArgumentValues constructorArguments = beanDefinition.getConstructorArgumentValues();
			String[] existing = (String[]) constructorArguments.getIndexedArgumentValue(0, String[].class).getValue();
			constructorArguments.addIndexedArgumentValue(0, concatenateStringArrays(existing, packageNames));
		} else {
			GenericBeanDefinition beanDefinition = new GenericBeanDefinition();
			beanDefinition.setBeanClass(FrontendScanPackages.class);
			beanDefinition.getConstructorArgumentValues().addIndexedArgumentValue(0, packageNames);
			beanDefinition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
			registry.registerBeanDefinition(BEAN, beanDefinition);
		}
	}

	static class Registrar implements ImportBeanDefinitionRegistrar {
		@Override
		public void registerBeanDefinitions(AnnotationMetadata metadata, BeanDefinitionRegistry registry) {
			register(registry, getPackagesToScan(metadata));
		}

		private String[] getPackagesToScan(AnnotationMetadata metadata) {
			AnnotationAttributes attributes = AnnotationAttributes
					.fromMap(metadata.getAnnotationAttributes(FRONTEND_SCAN_NAME));
			Class<?>[] basePackageClasses = attributes.getClassArray("basePackageClasses");
			if (basePackageClasses.length == 0) {
				return new String[] { ClassUtils.getPackageName(metadata.getClassName()) };
			}
			return Arrays.stream(basePackageClasses).map(ClassUtils::getPackageName).toArray(String[]::new);
		}
	}
}