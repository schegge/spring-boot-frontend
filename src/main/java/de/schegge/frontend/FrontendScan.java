package de.schegge.frontend;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.AliasFor;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(FrontendScanPackages.Registrar.class)
public @interface FrontendScan {
	@AliasFor("basePackageClasses")
	Class<?>[] value() default {};

	@AliasFor("value")
	Class<?>[] basePackageClasses() default {};
}
