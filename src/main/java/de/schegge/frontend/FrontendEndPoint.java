package de.schegge.frontend;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.Selector;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;

import de.schegge.frontend.feature.AnnotationFeature;

@Component
@Endpoint(id = "frontend")
public class FrontendEndPoint {

  private static final Logger LOGGER = LoggerFactory.getLogger(FrontendEndPoint.class);

  private final Map<String, List<ConstraintInfo>> infos = new HashMap<>();
  private final FrontendProperties properties;

  @Autowired
  public FrontendEndPoint(ApplicationContext applicationContext, List<AnnotationFeature> validationFeatures,
      FrontendProperties properties) {
    LOGGER.debug("features: ", validationFeatures);
    this.properties = properties;
    LOGGER.debug("messages: ", properties.isAddMessage());

    createInfo(applicationContext, validationFeatures);
  }

  private void createInfo(ApplicationContext applicationContext, List<AnnotationFeature> validationFeatures) {
    Validations validations = new Validations();
    validations.setAddMessage(properties.isAddMessage());
    validationFeatures.stream().forEach(validations::addFeature);
    Set<Class<?>> frontendClasses = new FrontendScanner(applicationContext).scan();
    for (Class<?> frontendClass : frontendClasses) {
      LOGGER.info("frontend annotated class: {}", frontendClass);
      String name = createName(frontendClass);
      infos.put(name, validations.read(frontendClass));
    }
  }

  private String createName(Class<?> frontendClass) {
    String name = AnnotationUtils.findAnnotation(frontendClass, Frontend.class).value();
    return name.isEmpty() ? frontendClass.getSimpleName() : name;
  }

  @ReadOperation
  public Map<String, List<ConstraintInfo>> frontend() {
    return infos;
  }

  @ReadOperation
  public List<ConstraintInfo> singleFrontend(@Selector String name) {
    return infos.get(name);
  }
}