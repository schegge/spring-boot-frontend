package de.schegge.frontend;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.schegge.frontend.feature.AnnotationFeature;
import de.schegge.frontend.feature.ValidationsFeature;

@Configuration
public class ValidationsConfiguration {
  @Bean("validations")
  AnnotationFeature getFeature() {
    return new ValidationsFeature();
  }
}
