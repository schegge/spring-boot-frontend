package de.schegge.frontend;

import javax.validation.executable.ExecutableValidator;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ConditionalOnClass(ExecutableValidator.class)
@Import({ HibernateConfiguration.class, ValidationsConfiguration.class, UriValidatorConfiguration.class,
    FrontendEndPoint.class })
@EnableConfigurationProperties(FrontendProperties.class)
public class FrontendAutoConfiguration {
}
