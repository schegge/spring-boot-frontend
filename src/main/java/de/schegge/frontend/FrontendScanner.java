package de.schegge.frontend;

import static java.util.stream.Collectors.toSet;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.autoconfigure.AutoConfigurationPackages;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.util.ClassUtils;
import org.springframework.util.StringUtils;

public class FrontendScanner {

  private final ApplicationContext context;

  public FrontendScanner(ApplicationContext context) {
    this.context = context;
  }

  public final Set<Class<?>> scan() {
    List<String> packages = getPackages();
    ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
    scanner.setEnvironment(context.getEnvironment());
    scanner.setResourceLoader(context);
    scanner.addIncludeFilter(new AnnotationTypeFilter(Frontend.class));
    return packages.stream().filter(StringUtils::hasText).map(scanner::findCandidateComponents).flatMap(Set::stream)
        .map(this::mapToClass).filter(Objects::nonNull).collect(toSet());
  }

  private Class<?> mapToClass(BeanDefinition candidate) {
    return ClassUtils.resolveClassName(candidate.getBeanClassName(), context.getClassLoader());
  }

  private List<String> getPackages() {
    List<String> packages = FrontendScanPackages.get(context).getPackageNames();
    if (!packages.isEmpty()) {
      return packages;
    }
    return AutoConfigurationPackages.get(context);
  }
}