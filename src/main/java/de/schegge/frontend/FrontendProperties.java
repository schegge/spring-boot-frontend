package de.schegge.frontend;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "de.schegge.frontend")
public class FrontendProperties {
  private boolean addMessage;

  public boolean isAddMessage() {
    return addMessage;
  }

  public void setAddMessage(boolean addMessage) {
    this.addMessage = addMessage;
  }

}
