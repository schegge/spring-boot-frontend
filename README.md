[![pipeline status](https://gitlab.com/schegge/spring-boot-frontend/badges/master/pipeline.svg)](https://gitlab.com/schegge/spring-boot-frontend/commits/master)
[![coverage report](https://gitlab.com/schegge/spring-boot-frontend/badges/master/coverage.svg)](https://schegge.gitlab.io/spring-boot-frontend)


# Spring Boot Frontend

Spring Boot service, to expose Bean Validations to the front end via an actuactor endpoint.
The code is part of the blog article [Frontend Validierung mit Spring Boot (Teil 2)](https://schegge.de/2019/09/18/frontend-validierung-mit-spring-boot-teil-2/).

## Usage

Annotate all your DTO classes, which needs validation information in the frontend.

> **Annotated DTO**
> ```java
> @Frontend
> public class PersonDto {
>   @NonEmpty
>   private String firstname;
> }

The actuator endpoint ``/actuator/frontend`` returns the informationen

> **Actuator response**
> ```json
> [...
> {
>  "PersonDto": [ { "name": "firstname", "type": "String", "nonEmpty": true } ]
> }
> ...]

The actuator endpoint ``/actuator/frontend/PersonDto`` returns the informationen

> **Actuator response**
> ```json
> {
>  "PersonDto": [ { "name": "firstname", "type": "String", "nonEmpty": true } ]
> }


## Configuration

### Frontend Name
> **Annotated DTO with configurated name**
> ```java
> @Frontend("person")
> public class PersonDto {
>   @NonEmpty
>   private String firstname;
> }


The actuator endpoint ``/actuator/frontend`` returns the informationen

> **Actuator response**
> ```json
> [...
> {
>  "person": [ { "name": "firstname", "type": "String", "nonEmpty": true } ]
> }
> ...]

The actuator endpoint ``/actuator/frontend/person`` returns the informationen

> **Actuator response**
> ```json
> {
>  "person": [ { "name": "firstname", "type": "String", "nonEmpty": true } ]
> }

### Messages

> **Add validation messages to the output**
> ```properties
> # add to application.properties 
> de.schegge.frontend.add-message=true

The actuator endpoint ``/actuator/frontend/person`` returns the informationen

> **Actuator response**
> ```json
> {
>  "person": [ { "name": "firstname", "type": "String", "nonEmpty": true, 
>    "message": "{javax.validation.constraints.NotNull.message}" } ]
> }


##  Supported Annotations

### JSR 380

| Annotation  | JSON fragment |
| ------ | ------ |
| ``javax.validation.constraints.Email`` | ``"format": "Email", "regexp": String, "flags": Flag[]`` |
| ``javax.validation.constraints.Future`` | ``"future": true`` |
| ``javax.validation.constraints.FutureOrPresent`` | ``future": true, "present": true`` |
| ``javax.validation.constraints.Max`` | ``"max": long`` |
| ``javax.validation.constraints.Min`` | ``"min": long`` |
| ``javax.validation.constraints.Negative`` | ``"max": -1`` |
| ``javax.validation.constraints.NegativeOrZero`` | ``"max": 0`` |
| ``javax.validation.constraints.NotBlank`` | ``"nonEBlank": true`` |
| ``javax.validation.constraints.NotEmpty`` | ``"nonEmpty": true`` |
| ``javax.validation.constraints.NotNull`` | ``"nonNull": true`` |
| ``javax.validation.constraints.Past`` | ``"past": true`` |
| ``javax.validation.constraints.PastOrPresent`` | ``past": true, "present": true`` |
| ``javax.validation.constraints.Pattern`` | ``"format": "Pattern", "regexp": String, "flags": Flag[]`` |
| ``javax.validation.constraints.Positive`` | ``"min": 1`` |
| ``javax.validation.constraints.PositiveOrZero`` | ``"min": 0`` |
| ``javax.validation.constraints.Size`` | ``"min": int, "max": int`` |

### Hibernate

| Annotation  | JSON fragment |
| ------ | ------ |
| ``org.hibernate.validator.constraints.CreditCardNumber`` | ``"format": "CreditCardNumber", "ignoreNonDigitCharacters": boolean`` |
| ``org.hibernate.validator.constraints.EAN`` | ``"format": "EAN", "ignoreNonDigitCharacters": boolean`` |
| ``org.hibernate.validator.constraints.Email`` | ``"format": "Email", "regexp": String, "flags": Flag[]`` |
| ``org.hibernate.validator.constraints.ISBN`` | ``"format": "ISBN", "type": Type`` |
| ``org.hibernate.validator.constraints.Length`` | ``"min": int, "max": int`` |
| ``org.hibernate.validator.constraints.NotBlank`` | ``"nonBlank": true`` |
| ``org.hibernate.validator.constraints.NotEmpty`` | ``"nonEmpty": true`` |

### URI Validator

| Annotation  | JSON fragment |
| ------ | ------ |
| ``de.schegge.validator.Host`` | ``"host": String, "negate": boolean`` |
| ``de.schegge.validator.Localhost`` | ``"host": "localhost"`` |
| ``de.schegge.validator.NotLocalhost`` | ``"host": "localhost", "negate": true`` |
